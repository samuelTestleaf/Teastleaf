package week2.day2;

public interface TeleCom {
	//dial with number
	public void dialCaller(int phoneNumber);
	//dial with name
	public void dialCaller(String name); 
	// add contacts
	public void addContact(String name);

}
