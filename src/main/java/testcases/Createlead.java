package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class Createlead extends ProjectMethods{

	@BeforeClass
	public void setData() {
		 testCaseName = "TC001";
		 testCaseDescription = "Create a lead";
		category = "Smoke";
		author= "sam";
		filename = "createlead";
	}

	@Test(dataProvider = "createLead") //invocationCount = 2, invocationTimeOut=30000)
	public void createLead(String cname, String fname, String lname) {
		//login();
		WebElement eleCrateLead = locateElement("linktext","Create Lead");
		click(eleCrateLead);
		WebElement eleCN = locateElement("id","createLeadForm_companyName");
		type(eleCN,cname);
		WebElement eleFN = locateElement("id","createLeadForm_firstName");
		type(eleFN,fname);
		WebElement eleLN = locateElement("id","createLeadForm_lastName");
		type(eleLN,lname);
		WebElement eleCreateLeadButton = locateElement("class","smallSubmit");
		click(eleCreateLeadButton);

	}
	

}







