package testcases;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearingReports {
	public static ExtentReports report;
	public static String testCaseName, testCaseDescription, author, category, filename;
	
	
	@BeforeSuite
	public void report()
	{
		ExtentHtmlReporter html  = new  ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);	
		report = new ExtentReports();
		report.attachReporter(html);
	}
	
	@BeforeMethod
	public void reportData()
	{
		ExtentTest createTest = report.createTest(testCaseName, testCaseDescription);	
		createTest.assignAuthor(author);
		createTest.assignCategory(category);
	}
	
	@AfterSuite
	
	public void action()
	{
		report.flush();	
	}
}