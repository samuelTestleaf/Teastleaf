package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods {

	@BeforeClass
	public void setData() {
		testCaseName = "TC004";
		testCaseDescription = " Delete lead";
		category = "regression";
		author= "sam";
	}

	@Test(groups="regression", dependsOnGroups="sanity")//dependsOnMethods  ="testcases.Createlead.createLead", alwaysRun=false)
	public void deleteLead() throws InterruptedException {
		//login();
		WebElement leadmenu = locateElement("linktext", "Leads");
		clickWithNoSnap(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		clickWithNoSnap(findlead);
		WebElement tapPhnTab = locateElement("xpath", "(//a[@class = 'x-tab-right'])[2]");
		clickWithNoSnap(tapPhnTab);
		WebElement PhnAreaCode = locateElement("xpath", "//input[@name = 'phoneAreaCode']");
		type(PhnAreaCode, "11");
		WebElement PhnNmbr = locateElement("xpath", "//input[@name = 'phoneNumber']");
		type(PhnNmbr, "8939063459");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		clickWithNoSnap(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		String getID = pickalead.getText();
		clickWithNoSnap(pickalead);
		WebElement deletelink = locateElement("linktext", "Delete");
		clickWithNoSnap(deletelink);
		WebElement findDeletedLead = locateElement("xpath", "//a[text() = 'Find Leads']");
		clickWithNoSnap(findDeletedLead);
		WebElement LeadID = locateElement("xpath", "//input[@name = 'id']");
		type(LeadID, getID);
		WebElement filtrleadagain = locateElement("xpath", "//button[text() = 'Find Leads']");
		clickWithNoSnap(filtrleadagain);
		closeBrowser();




	}
}
