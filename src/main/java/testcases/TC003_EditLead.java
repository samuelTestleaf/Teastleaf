package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{

	@BeforeClass
	public void setData() {
		 testCaseName = "TC003";
		 testCaseDescription = "Edit lead";
		category = "sanity";
		author= "sam";
	}
	@Test(groups="sanity", dependsOnGroups="smoke")//dependsOnMethods  ="testcases.Createlead.createLead")
	public void editlead() throws InterruptedException {
		//login();
		WebElement leadmenu = locateElement("linktext", "Leads");
		clickWithNoSnap(leadmenu);
		WebElement findlead = locateElement("xpath", "//a[text() = 'Find Leads']");
		clickWithNoSnap(findlead);
		WebElement leadname = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
		type(leadname, "Dinesh");
		WebElement filtrlead = locateElement("xpath", "//button[text() = 'Find Leads']");
		clickWithNoSnap(filtrlead);
		Thread.sleep(5000);
		WebElement pickalead = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		click(pickalead);
		String expectedTitle = driver.getTitle();
		verifyTitle(expectedTitle);
		WebElement editlink = locateElement("linktext", "Edit");
		clickWithNoSnap(editlink);
		WebElement updtcompname = locateElement("id", "updateLeadForm_companyName");
		updtcompname.clear();
		String text = "newcompanyname1";
		type(updtcompname, text);
		WebElement updatelead = locateElement("xpath", "//input[@value = 'Update']");
		clickWithNoSnap(updatelead);
		WebElement updatedcmpnyname = locateElement("id", "viewLead_companyName_sp");
		String Actualcmpnyname = updatedcmpnyname.getText();
		//Verify the Expected & Actual
		if(Actualcmpnyname.contains(text))
		{
			System.out.println("Verification of Company Name is Success");
		}else
			System.out.println("Verification of Company Name is Failed");
		closeBrowser();
	}
}