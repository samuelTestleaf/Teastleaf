package testcases;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel  {

	public static Object[][] excelData(String filename) throws IOException {

		
			XSSFWorkbook wbook = new XSSFWorkbook("./data/"+filename+".xlsx");
			XSSFSheet sheet = wbook.getSheetAt(0);
			int lastRowNum = sheet.getLastRowNum();
			short lastCellNum = sheet.getRow(0).getLastCellNum();
			Object[][] data = new Object[lastRowNum][lastCellNum]; 
			for(int i=1; i<=lastRowNum; i++)
			{
				XSSFRow row = sheet.getRow(i);
				for(int j =0; j<lastCellNum; j++)
				{
					XSSFCell cell = row.getCell(j) ;
					String stringCellValue = cell.getStringCellValue();
					data[i-1][j]=stringCellValue;
					System.out.println(stringCellValue);
				}
			}
			return data;
		}
	
	}
