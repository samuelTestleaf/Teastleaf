package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {

	@Test
	public void createLead() {
		//login();
		WebElement elesel1 = locateElement("linktext", "Create Lead");
		click(elesel1);
		WebElement elesel2 = locateElement("id", "createLeadForm_companyName");
		type(elesel2, "HCL");
		WebElement elesel3 = locateElement("id", "createLeadForm_firstName");
		type(elesel3, "SAM");
		WebElement elesel4 = locateElement("id", "createLeadForm_lastName");
		type(elesel4, "Deva");
		WebElement elesour = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(elesour,"Cold Call");
		WebElement elesour1 = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingText(elesour1,"Affiliate Sites");
		WebElement elecrelead = locateElement("class","smallSubmit");
		click(elecrelead);
		/*WebElement elesele = locateElement("linktext", "Merge Leads");
		click(elesele);
		WebElement elesel8 = locateElement("xpath","//input[@id='partyIdFrom']/following::img");
		click(elesel8);
		switchToWindow(1);
		WebElement elename = locateElement("name","id");
		type(elename, "10438");
		WebElement click1 = locateElement("class","x-btn-text");
		click(click1);
		WebElement click2 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(click2);
		switchToWindow(0);
		WebElement eles5 = locateElement("xpath","//input[@id='partyIdTo']/following::img");
		click(eles5);
		switchToWindow(1);
		WebElement ele1 = locateElement("name","id");
		type(ele1, "10440");	
		WebElement ele2 = locateElement("class","x-btn-text");
		click(ele2);
		WebElement click3 = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"); 
		clickWithNoSnap(click3);
		switchToWindow(0);
		WebElement click4 = locateElement("linktext", "Merge");
		clickWithNoSnap(click4);
		//System.out.println(driver.getTitle());
		acceptAlert();

		 */
	}
}










