package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Viewlead extends ProjectMethods {
	public Viewlead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Edit")
	WebElement  eleclickEdit;
	public EditLead clickEdit()
	{
		//WebElement eleclickEdit = locateElement("linktext", "Edit");
		click(eleclickEdit);
		return new EditLead();
	}
	@FindBy(linkText="Delete")
	WebElement  eleclickDelete;
	public MyLeadsPage clickDelete()
	{
		click(eleclickDelete);
		return new MyLeadsPage();
	}
	@FindBy(linkText="Duplicate Lead")
	WebElement  eleClickDuplicate;
	public DuplicateLead clickDuplicate()
	{
		click(eleClickDuplicate);
		return new DuplicateLead();
	}
	@FindBy(id="viewLead_firstName_sp")
	WebElement  eleVerify;
	public FindLeads verifyName()
	{
		String text = eleVerify.toString();
		FindLeads verify= new FindLeads();
		verify.findLeadName(text);
		return new FindLeads();
	}

}
