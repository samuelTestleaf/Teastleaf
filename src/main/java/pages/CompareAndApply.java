package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class CompareAndApply extends SeMethods{

	public CompareAndApply()
	{
		PageFactory.initElements(driver, this);
	}
@And("ClickInvesments")
	public CompareAndApply ClickInvesments()
	{
		WebElement eleClickInvesments = locateElement("class", "investments-menu");
		click(eleClickInvesments);
        return this;   
	}
@And("mouseOver")
	public MutualFunds mouseOver()
	{
		WebElement eleMouseOver = locateElement("linktext", "Mutual Funds");
		Actions ee = new Actions(driver);
		ee.moveToElement(eleMouseOver).click().perform();
		return new MutualFunds();
	}



}
