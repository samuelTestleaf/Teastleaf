package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class MutualFunds extends SeMethods{
	public MutualFunds()
	{
		PageFactory.initElements(driver, this);
	}
	@And("clickSearchMutual")
	public MutualFunds clickSearchMutual() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement eleClickSearchMutual = locateElement("linktext", "Search for Mutual Funds");
		click(eleClickSearchMutual);
		return this;   

	}
	@And("selectAge")
	public MutualFunds selectAge()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleSelectAge = locateElement("xpath", "//div[@class='rangeslider__handle']");
		Actions ee = new Actions(driver);
		for (int i = 0; i < 7; i++) {
			ee.dragAndDropBy(eleSelectAge, 5, 0).perform();
		}
		return this;
		
	}
	@And("selectYear")
	public MutualFunds selectYear()
	{
		WebElement eleSelectYear = locateElement("linktext", "Jun 1993");
		click(eleSelectYear);
		return this;
	}
	@And("selectDate")
	public MutualFunds selectDate()
	{

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleSelectDate = locateElement("xpath", "//*[text()='19']");
		click(eleSelectDate);
		return this;
	}

	@And("clickContinue")
	public MutualFundsAnnualIncome clickContinue()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleClickContinue = locateElement("linktext", "Continue");
		click(eleClickContinue);
		return new MutualFundsAnnualIncome();
	}


	




}
