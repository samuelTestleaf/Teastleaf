package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLead extends ProjectMethods{

	public DuplicateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name="submitButton")
	WebElement  eleClicksubmitButton;
	public Viewlead ClicksubmitButton()
	{
		click(eleClicksubmitButton);
		return new Viewlead();
	}
	

}
