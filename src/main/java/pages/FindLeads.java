package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	public FindLeads()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//span[text()='Phone']")
	WebElement eleFindPhoneNo;
	@FindBy(name="phoneNumber")
	WebElement eleTypeNo;	
	public FindLeads FindPhoneNo(String phnNumber)
	{
		//WebElement eleFindPhoneNo = locateElement("xpath", "//span[text()='Phone']");
		click(eleFindPhoneNo);
		//type(locateElement("name", "phoneNumber"),phnNumber);
		type(eleTypeNo, phnNumber);
		return this;
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeads;
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement elelocateNo;	
	public Viewlead clickFindLeads()
	{
		//WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//WebElement elelocateNo = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(elelocateNo);  
		return new Viewlead();
	}
	public FindLeads clickFindLeadsContinue()
	{
		//WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//WebElement elelocateNo = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(elelocateNo);  
		return this;
	}

	@FindBy(xpath="//span[text()='Email']")
	WebElement eleFindEmail;
	public FindLeads findEmail()
	{
		click(eleFindEmail);
		return this;
	}
	@FindBy(name="emailAddress")
	WebElement eleTypeEmail;
	public FindLeads typeEmail(String email)
	{
		type(eleTypeEmail, email);
		return this;
	}
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleLeadId;
	public FindLeads leadId()
	{
		String leadId = eleLeadId.getText(); 
		return this;
	}
	public Viewlead click()
	{
		click(eleLeadId); 
		return new Viewlead();
	}
	@FindBy(name="id")
	WebElement eleTypeId;
	public FindLeads findId()
	{ 
		String text = eleLeadId.getText();
		type(eleTypeId, text);
		return this;
	}
	@FindBy(className="x-paging-info")
	WebElement eleVerify;
	public Viewlead verify(String errorMsg)
	{
		verifyExactText(eleVerify, errorMsg);
		return new Viewlead();
	}
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]")
	WebElement elefindLeadName;
	public Viewlead findLeadName(String text2)
	{

		verifyExactText(elefindLeadName, text2);
		return new Viewlead();
	}
	@FindBy(xpath="//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]")
	WebElement eleClickResult;
	public Viewlead  clickResult()
	{
		click(eleClickResult);
		return new Viewlead();
	}

}


