package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class InvestIn extends SeMethods {

	public InvestIn()

	{
		PageFactory.initElements(driver, this);
	}
	@And("print Scheme Name")
	public InvestIn printSchemeName()
	
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<WebElement> schemes = driver.findElementsByClassName("js-offer-name");
		for (WebElement scheme : schemes) {
			System.out.println("Scheme Names "+scheme.getText());
			
		}
		return this;
	}
	@And("print Investment Amount")
	public InvestIn printInvestmentAmount()
	
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<WebElement> investments = driver.findElementsByXPath("//div[@class='offer-section-column col-same-height col-middle investment-amount']//span[@class='js-title']");
		for (WebElement investment : investments) {
			System.out.println("Investment Amount "+investment.getText());
			
		}
		return this;
	}

	

}
