package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class MutualFundsBank extends SeMethods {

	public MutualFundsBank()
	{
		PageFactory.initElements(driver, this);
	}
	@And("ClickBank")
	public MutualFundsName clickSearchMutual() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement eleClickBank = locateElement("class", "PrimaryAccount_iconIciciBank_1Bf7g");
		click(eleClickBank);
		return new MutualFundsName();   

	}
//	@And("clickContinueBank")
//	public MutualFundsName clickContinueBank()
//	{
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		WebElement eleClickBank = locateElement("linktext", "Continue");
//		click(eleClickBank);
//		return new MutualFundsName();
//	}


}

