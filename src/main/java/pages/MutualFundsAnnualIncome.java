package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class MutualFundsAnnualIncome extends SeMethods{
	public MutualFundsAnnualIncome()

	{
		PageFactory.initElements(driver, this);
	}
	@And("selectIncome")
	public MutualFundsAnnualIncome selectIncome()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleSelectIncome = locateElement("xpath", "//div[@class='rangeslider__handle']");
		Actions ee = new Actions(driver);
		for (int i = 0; i < 5; i++) {
			ee.dragAndDropBy(eleSelectIncome, 5, 0).click().perform();
		}
		return this;
	}
	@And("clickContinueInIncome")
	public MutualFundsName clickContinueInIncome()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleClickContinue = locateElement("linktext", "Continue");
		click(eleClickContinue);
		return new MutualFundsName();
	}


	
}
