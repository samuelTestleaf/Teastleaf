package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.SeMethods;

public class MutualFundsName extends SeMethods{
	public MutualFundsName()

	{
		PageFactory.initElements(driver, this);
	}
	@And("tpeName")
	public InvestIn ClickName()
	
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleName = locateElement("xpath","//input[@type='text']");
		type(eleName, "sam");
		return new InvestIn();
	}
//	@And("EnterName")
//	public MutualFundsName enterName() 
//	{
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		WebElement eleEnterName = locateElement("xpath", "//input[@name='firstName']");
//		
//		return this;   
//
//	}
	
	@And("View Mutual Funds")
	public MutualFundsName clickViewMutualFunds()
	{
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement eleViewMutualFunds = locateElement("linktext", "View Mutual Funds");
		click(eleViewMutualFunds);
		return this;
			
	}
}
