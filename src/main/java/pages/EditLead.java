package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.SeMethods;

public class EditLead extends SeMethods{
	public EditLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="updateLeadForm_companyName")
	WebElement elecName;
	public EditLead editCompanyName(String cname)
	{
		//type(locateElement("id", "updateLeadForm_companyName"),cname);
		type(elecName, cname);
		return this;
	}
	@FindBy(className="smallSubmit")
	WebElement eleClickSubmit;
	public EditLead clickSubmit()
	{
		//click(locateElement("class", "smallSubmit"));
		click(eleClickSubmit);
		return this;
	}
}