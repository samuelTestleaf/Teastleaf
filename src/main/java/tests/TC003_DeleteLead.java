//package tests;
//
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//import pages.MyHomePage;
//import wdMethods.ProjectMethods;
//
//public class TC003_DeleteLead extends ProjectMethods {
//	@BeforeClass()
//	public void setData() {
//		testCaseName = "TC003_DeleteLead";
//		testCaseDescription ="Delete a lead";
//		category = "Smoke";
//		author= "Babu";
//		filename="TC003";
//	}
//	@Test(dataProvider="fetchData")
//	public  void deleteLead(String email, String errorMsg) throws InterruptedException   {
//           new MyHomePage()
//           .clickLeads()
//           .findLeads()
//           .findEmail()
//           .typeEmail(email)
//           .clickFindLeadsContinue()
//           .click()
//           .clickDelete()
//           .findLeads()
//           .findId()
//           .clickFindLeadsContinue()
//           .verify(errorMsg);
//           
//           
//
//	}
//
//}
