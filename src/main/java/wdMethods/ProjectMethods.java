package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import testcases.ReadExcel;

public class ProjectMethods extends SeMethods{

	@DataProvider(name = "filename")
	public Object[][] data() throws IOException
	{
		Object[][] excelData = ReadExcel.excelData(filename);	
      return excelData;
	}
	@Parameters({"url","uname","pwd"})
	@BeforeMethod()
	public void login(String url,String uname, String pwd) {
		startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement elesel = locateElement("linktext", "CRM/SFA");
		click(elesel);
	}
	@AfterMethod()
	public void close()
	{
		closeAllBrowsers();
	}	
}


