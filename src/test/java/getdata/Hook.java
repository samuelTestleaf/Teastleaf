package getdata;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hook extends SeMethods {

	@Before
	public void beforeCucumber(Scenario sc)
	{
		startResult();
		testCaseName = sc.getName();
		testCaseDescription=sc.getId();
		author="sam";
		category="smoke";
		startTestCase();
		startApp("chrome", "https://www.bankbazaar.com/");
		//System.out.println(sc.getName());
		//System.out.println(sc.getId());
	}
	@After
	public void afterCucumber(Scenario sc)
	{
	//	closeAllBrowsers();
		stopResult();
		//System.out.println(sc.getStatus());
	}
}
