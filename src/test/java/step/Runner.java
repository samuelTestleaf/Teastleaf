package step;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(features="src/test/java/features/BankBazaar.feature", glue= {"pages","getdata"}, monochrome=true)
@RunWith(Cucumber.class)
public class Runner {

}
