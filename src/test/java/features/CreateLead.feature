Feature: Create Lead 
Scenario Outline: Posiivie Flow
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And Click Login
And Click CRMSFA
And Click Create Lead
And Enter companyName as <companyName>
And Enter firstName as <firstName>
And Enter last name as <last name>
Then Click submitButton
Examples: 
|companyName|firstName|last name|
|HCL|sam|deva|
|HCL|samuel|deva|

Scenario Outline: Negative Flow
And Enter the username as DemoSalesManager
And Enter the password as crmsf
And Click Login
And Click CRMSFA
And Click Create Lead
And Enter companyName as <companyName>
And Enter firstName as <firstName>
And Enter last name as <last name>
But Click submitButton
Examples: 
|companyName|firstName|last name|
|HCL|sam|deva|
|HCL|samuel|deva| 